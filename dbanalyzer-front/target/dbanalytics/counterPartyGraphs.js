 $(document).ready(function() { 
 	var graphMade = false;
 	$("#counterPartyDropDown").select();
 	$.ajax({
    method: 'GET',
    url: "counterPartyList.jsp",
    success: function(msg) {
    	if(msg == "error") {
        console.log(msg);
      } else {
        var counterPartyDropList = JSON.parse(msg);
        $.each(counterPartyDropList, function(i) {
          $("#counterPartyDropDown").append('<option value="'+this.counterparty_name+'">'+this.counterparty_name+'</option>');
        }); 

      }
    },
    error: function(msg) {
      console.log("didnt work");
    }
  });
   $('#counterPartyDropDown').select2({
    placeholder: 'Select a counterparty!'
  }).on('change', function() {
  	var counterPartyName = $("#counterPartyDropDown option:selected").text();
    $.ajax({
      method: 'GET',
      url: "portfolioDetails.jsp",
      data: "counterParty="+counterPartyName+"&dealType=B",
      dataType: "json",
      success: function(msg) {
        if(msg == "error") {
          console.log(msg);
        } else {
          msg.sort( function( a, b ) {
            return a.instrument_name < b.instrument_name ? -1 : a.instrument_name > b.instrument_name ? 1 : 0;
          });
          var buyPieValues = [];
          var buyPieLabels = [];
          for(var i = 0; i < msg.length; i++) {
            buyPieValues[i] = msg[i].quality;
            buyPieLabels[i] = msg[i].instrument_name;
          }
          var data = [{
            values: buyPieValues,
            labels: buyPieLabels,
            type: 'pie',
            textfont: {
              color: '#FFFFFF'
            },
            sort: false
          }];

          var layout = {
            title: 'Buy',
            titlefont: {
              size: 26,
            },
            height: 500,
            width: 500,
            sort: false
          };

          Plotly.newPlot('portfolioPieBuy', data, layout);
        }
      },
      error: function(msg) {
        console.log("didnt work");
      }
    });
    $.ajax({
      method: 'GET',
      url: "portfolioDetails.jsp",
      data: "counterParty="+counterPartyName+"&dealType=S",
      dataType: "json",
      success: function(msg) {
        if(msg == "error") {
          console.log(msg);
        } else {
          msg.sort( function( a, b ) {
            return a.instrument_name < b.instrument_name ? -1 : a.instrument_name > b.instrument_name ? 1 : 0;
          });
          var sellPieValues = [];
          var sellPieLabels = [];
          for(var i = 0; i < msg.length; i++) {
            sellPieValues[i] = msg[i].quality;
            sellPieLabels[i] = msg[i].instrument_name;
          }
          var data = [{
            values: sellPieValues,
            labels: sellPieLabels,
            type: 'pie',
            textfont: {
              color: '#FFFFFF'
            },
            sort: false
          }];

          var layout = {
            title: 'Sell',
            titlefont: {
              size: 26,
            },
            height: 500,
            width: 500,
            sort: false
          };

          Plotly.newPlot('portfolioPieSell', data, layout);
        }
      },
      error: function(msg) {
        console.log("didnt work");
      }
    });     
    $("#counterPartyGraphTitle").text("Average Buy and Sell Prices");
    $.ajax({
      method: 'GET',
      url: "getCounterPartyDetails.jsp",
      data: "counterParty="+counterPartyName,
      dataType: "json",
      success: function(msg) {
          
          if(msg == "error") {
            console.log(msg);
          } else {
            var instrumentNameArr = [];
            var buyPriceArr = [];
            var sellPriceArr = [];
            for(var i = 0; i < msg.length; i++) {
              instrumentNameArr[i] = msg[i].instrument_name;
              buyPriceArr[i] = msg[i].buy;
              sellPriceArr[i] = msg[i].sell
            }

            var trace1 = {
              x: instrumentNameArr,
              y: buyPriceArr,
              name: 'Buy',
              type: 'bar'
            };
            var trace2 = {
              x: instrumentNameArr,
              y: sellPriceArr,
              name: 'Sell',
              type: 'bar'
            };
            var data = [trace1, trace2];
            var layout = {
              barmode: 'group',
              title: 'Average Prices',
              titlefont: {
                size: 26,
              }
            };
            Plotly.newPlot('buySellCounterParty', data, layout);
          }
        },
        error: function(msg) {
          console.log("didnt work");
        }
      });
    $.ajax({
      method: 'GET',
      url: "netTradesCounterParty.jsp",
      data: "counterParty="+counterPartyName,
      dataType: "json",
      success: function(msg) {
        if(msg == "error") {
          console.log(msg);
        } else {
          var instrumentNameArrPositive = [];
          var netQuantityArrPositive = [];
          var instrumentNameArrNegative = [];
          var netQuantityArrNegative = [];
          for(var i = 0; i < msg.length; i++) {
            if(msg[i].net_quantity > 0) {
              instrumentNameArrPositive[i] = msg[i].instrument_name;
              netQuantityArrPositive[i] = msg[i].net_quantity;
            } else if (msg[i].net_quantity < 0) {
              instrumentNameArrNegative[i] = msg[i].instrument_name;
              netQuantityArrNegative[i] = msg[i].net_quantity;
            } else {
              console.log("error");
            }

          }
          var data = [
          {
            x: instrumentNameArrPositive,
            y: netQuantityArrPositive,
            type: 'bar',
            marker: {
              color: '#1f77b4'
            },
            name: 'Long'

          },
          {
            x: instrumentNameArrNegative,
            y: netQuantityArrNegative,
            type: 'bar',
            marker: {
              color: '#ff7f0e'
            },
            name: 'Short'
          }
          ];

          var layout = {

            title: 'End Positions',
            titlefont: {
              size: 26,
            }
          };

          Plotly.newPlot('netTradesCounterParty', data, layout);

        }
      },
      error: function(msg) {
        console.log("didnt work");
      }
    });
    setTimeout(function(){ 
      $("#counterPartyGraph").css("display", "block");
    }, 1000);
  
  });
});
