package com.db.demomidtier;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServlet;

import org.json.JSONArray;
import org.json.JSONObject;

public class UserData extends HttpServlet {

    private Utils utils;
    private String viewName;

    public UserData() {
        utils = new Utils();
        viewName = utils.getViewName();
    }

    public UserData(Connection connection) {
        utils = new Utils(connection);
        viewName = utils.getViewName();
    }

    public String getNetTradesByCP(String counterPartyName) {
        String queryString = String.format(GET_NET_TRADES_CP, viewName);
        return getDataFromDb(queryString, counterPartyName);
    }

    public String getAveragePrices() {
        String queryString = String.format(GET_AVERAGE_PRICES_QUERY, viewName);
        return getDataFromDb(queryString);
    }

    public String getRealizedProfits() {
        String queryString = String.format(GET_REALIZED_PROFIT_QUERY, viewName);
        return getDataFromDb(queryString);
    }

    public String getInstrumentDetails(String instrument) {
        String queryString = String.format(GET_INSTR_DETAILS_QUERY, viewName);
        return getDataFromDb(queryString, instrument);
    }

    public String getCounterpartyDetails(String name) {
        String queryString = String.format(GET_COUNTERPARTY_DETAIL_QUERY, viewName);
        return getDataFromDb(queryString, name);
    }

    public String getCounterpartyList() {
        String queryString = String.format(GET_COUNTERPARTIES_QUERY, viewName);
        return getDataFromDb(queryString);
    }

    public String getInstrumentList() {
        String queryString = String.format(GET_INSTR_QUERY, viewName);
        return getDataFromDb(queryString);
    }

    public String getInstrumentPortfolioByCounterparty(String name, String type) {
        String queryString = String.format(GET_INSTR_PORTF_BY_COUNT, viewName, viewName);
        return getDataFromDb(queryString, name, type, name, type);
    }

    public String getAllDeals() {
        String queryString = String.format(GET_ALL_DEALS_QUERY, viewName);
        return getDataFromDb(queryString);
    }

    private String getDataFromDb(String queryString, String... param) {
        JSONArray resultArray = null;
        try {
            resultArray = utils.executeSQL(queryString, param);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        if (resultArray != null)
            return resultArray.toString();
        else
            return null;
    }

    public JSONObject getDealsTable(int draw, int offset, int limit, String orderColumn, boolean asc) {
        String queryString = "Select * from " + viewName + " ORDER BY " + orderColumn;
        if (!asc) {
            queryString += " DESC";
        }
        queryString += " LIMIT " + offset + "," + limit;

        JSONObject obj = new JSONObject();
        obj.put("draw", draw);
        try {
            obj.put("data", utils.executeSQL(queryString));
        } catch (SQLException e) {
            obj.put("error", "Error loading data from database");
            return obj;
        }

        int amountOfRecords = 0;
        try {
            String numOfRowsQuery = String.format(GET_NUM_ROWS_QUERY, viewName);
            JSONArray result = utils.executeSQL(numOfRowsQuery);
            if (result != null) {
                JSONObject numOfRowsObj = (JSONObject) result.get(0);
                amountOfRecords = numOfRowsObj.getInt("num_of_rows");
            }
        } catch (SQLException e) {
            obj.put("error", "Error getting the amount of rows");
            return obj;
        }

        obj.put("recordsTotal", amountOfRecords);
        obj.put("recordsFiltered", amountOfRecords);

        return obj;
    }

    public JSONArray getEffectiveProfits() {
        JSONArray result = new JSONArray();

        Map<String, Double> lastBuyPrices;
        Map<String, Double> lastSellPrices;
        try {
            lastBuyPrices = getLastPrices(true);
            lastSellPrices = getLastPrices(false);
        } catch (SQLException e) {
            return null;
        }

        String queryCounterparties = String.format(GET_COUNTERPARTIES_QUERY, viewName);
        try {
            JSONArray jsonArray = utils.executeSQL(queryCounterparties);
            if (jsonArray == null) return null;
            for (Object obj : jsonArray) {
                String counterparty = ((JSONObject) obj).getString("counterparty_name");

                Map<String, Double> avgBuyPrices;
                Map<String, Double> avgSellPrices;
                try {
                    avgBuyPrices = getAvgPrices(counterparty, true);
                    avgSellPrices = getAvgPrices(counterparty, false);
                } catch (Exception e) {
                    return null;
                }

                JSONObject jobj = new JSONObject();
                jobj.put("counterparty_name", counterparty);
                jobj.put("effective_profit", getEffectiveProfit(counterparty,
                        lastBuyPrices, lastSellPrices,
                        avgBuyPrices, avgSellPrices));
                result.put(jobj);
            }
        } catch (SQLException e) {
            return null;
        }

        return result;
    }

    public Double getEffectiveProfit(String counterparty,
                                     Map<String, Double> lastBuyPrices, Map<String, Double> lastSellPrices,
                                     Map<String, Double> avgBuyPrices, Map<String, Double> avgSellPrices) {
        String realizedProfitQuery = String.format(GET_REALIZED_PROFIT_PER_DEALER_QUERY, viewName);
        JSONArray arr;
        try {
            arr = utils.executeSQL(realizedProfitQuery, counterparty, counterparty);
        } catch (SQLException e) {
            return null;
        }
        if (arr == null) return null;
        if (arr.length() == 0) {
            return null;
        }
        Double realizedProfit = ((JSONObject) arr.get(0)).getDouble("ending_position");

        Map<String, Long> netQuantities;
        try {
            netQuantities = getDealerNetQuantities(counterparty);
        } catch (SQLException e) {
            return null;
        }
        if (netQuantities == null) return null;
        Double pr = 0.0;
        for (String instrument : netQuantities.keySet()) {
            if (netQuantities.get(instrument) > 0 && lastSellPrices != null) {
                pr += (lastSellPrices.get(instrument) - avgBuyPrices.get(instrument)) * netQuantities.get(instrument);
            } else if (lastBuyPrices != null) {
                pr += (lastBuyPrices.get(instrument) - avgSellPrices.get(instrument)) * (-netQuantities.get(instrument));
            }
        }

        return realizedProfit + pr;
    }

    Map<String, Long> getDealerNetQuantities(String dealerName) throws SQLException {
        Map<String, Long> result = new HashMap<>();

        String query = String.format(GET_DEALER_NET_QUANTITIES_QUERY, viewName);

        JSONArray arr = utils.executeSQL(query, dealerName);
        if (arr == null) return null;
        for (Object obj : arr) {
            JSONObject jobj = (JSONObject) obj;

            result.put(jobj.getString("instrument_name"), jobj.getLong("net_quantity"));
        }

        return result;
    }

    private Map<String, Double> getLastPrices(boolean isBuy) throws SQLException {
        String query = String.format(GET_LAST_PRICES_QUERY, viewName, viewName);

        Map<String, Double> result = new HashMap<>();

        JSONArray arr = utils.executeSQL(query, (isBuy) ? "B" : "S");
        if (arr == null) return null;
        for (Object obj : arr) {
            JSONObject jobj = (JSONObject) obj;
            result.put(jobj.getString("instrument_name"), jobj.getDouble("amount"));
        }

        return result;
    }

    Map<String, Double> getAvgPrices(String counterparty, boolean isBuy) throws SQLException {
        String query = String.format(GET_AVERAGE_PRICES_PER_DEALER_QUERY, viewName);

        Map<String, Double> result = new HashMap<>();

        JSONArray arr = utils.executeSQL(query, counterparty, (isBuy) ? "B" : "S");
        if (arr == null) return null;
        for (Object obj : arr) {
            JSONObject jobj = (JSONObject) obj;
            result.put(jobj.getString("instrument_name"), jobj.getDouble("avg_price"));
        }

        return result;
    }

    static String GET_NET_TRADES_CP = "Select a.counterparty_name Dealer,instrument_name, SUM(IF(deal_type =\"B\"," +
            "a.Quantity_sum,(-1)*a.Quantity_sum)) net_quantity " +
            "FROM( Select counterparty_name,deal_type,instrument_name, SUM(deal_quantity) Quantity_sum\n" +
            "From %s" +
            " Where counterparty_name =? Group by counterparty_name,instrument_name, deal_type ) as a " +
            "GROUP BY a.counterparty_name,instrument_name;";

    static String GET_AVERAGE_PRICES_PER_DEALER_QUERY = "select instrument_name, SUM(deal_amount * deal_quantity) / SUM(deal_quantity) as avg_price\n" +
            "from %s\n" +
            "where counterparty_name=? and deal_type=?\n" +
            "group by instrument_name";

    static String GET_AVERAGE_PRICES_QUERY = "SELECT instrument_name, sum( if( deal_type = 'B', ROUND(AVGg,2), 0 ) ) AS Buy, " +
            "sum( if( deal_type = 'S', ROUND(AVGg,2), 0 ) ) AS Sell " +
            "FROM (SELECT instrument_name,deal_type, AVG(deal_amount) as AVGg\n" +
            "FROM %s\n" +
            "Group by instrument_name,deal_type ) as a GROUP BY instrument_name;";

    static String GET_REALIZED_PROFIT_QUERY = "select b.counterparty_name as Dealer, Round(sum(b.profit),2) as Ending_Position from (\n" +
            "Select a.counterparty_name,a.instrument_name, MIN(q) * SUM(IF(deal_type =\"S\",a.avg_price,(-1)*a.avg_price)) as profit FROM\n" +
            "(\n" +
            "select counterparty_name, SUM(deal_quantity) as q, SUM(deal_quantity * deal_amount) / SUM(deal_quantity) as avg_price, instrument_name, deal_type\n" +
            "from %s\n" +
            "group by counterparty_name, instrument_name, deal_type) a\n" +
            "group by instrument_name,counterparty_name) b\n" +
            "group by b.counterparty_name\n" +
            "Order by 2 DESC;\n";

    static String GET_INSTR_DETAILS_QUERY = "SELECT dealTime, if( deal_type = 'B', ROUND(deal_amount,2), 0 ) AS Buy, " +
            "if( deal_type = 'S', ROUND(deal_amount,2), 0 ) AS Sell " +
            "FROM (SELECT deal_time as dealTime,deal_type, deal_amount\n" +
            "FROM %s" +
            " Where instrument_name=? ) as a ;";

    static String GET_COUNTERPARTY_DETAIL_QUERY = "SELECT instrument_name, sum( if( deal_type = 'B', ROUND(a.quantity,2), 0 )) AS Buy, " +
            "sum( if( deal_type = 'S', ROUND(a.quantity,2), 0 )) AS Sell " +
            "FROM( SELECT instrument_name, deal_type, sum(deal_quantity) as quantity " +
            "FROM %s" +
            " Where counterparty_name=? Group By 1,2) as a Group by instrument_name; ";

    static String GET_INSTR_QUERY = "Select distinct instrument_name from %s";

    static String GET_ALL_DEALS_QUERY = "Select * from %s";

    static String GET_INSTR_PORTF_BY_COUNT = "Select instrument_name, (Sum(deal_quantity) / (Select SUM(deal_quantity) as totalQuantity\n" +
            " from %s" +
            " where counterparty_name = ? and deal_type = ?))*100 as quality\n" +
            "from %s" +
            " where counterparty_name = ? and deal_type = ? Group by instrument_name;";

    static String GET_NUM_ROWS_QUERY = "select count(*) as num_of_rows from %s";

    static String GET_COUNTERPARTIES_QUERY = "select distinct counterparty_name from %s";

    static String GET_REALIZED_PROFIT_PER_DEALER_QUERY = "select b.counterparty_name as Dealer, Round(sum(b.profit),2) as Ending_Position from (\n" +
            "Select a.counterparty_name,a.instrument_name, MIN(q) * SUM(IF(deal_type =\"S\",a.avg_price,(-1)*a.avg_price)) as profit FROM\n" +
            "(\n" +
            "select counterparty_name, SUM(deal_quantity) as q, SUM(deal_quantity * deal_amount) / SUM(deal_quantity) as avg_price, instrument_name, deal_type\n" +
            "from %s\n" +
            "where counterparty_name=?\n" +
            "group by counterparty_name, instrument_name, deal_type) a\n" +
            "group by instrument_name,counterparty_name) b\n" +
            "where counterparty_name = ?\n" +
            "group by b.counterparty_name\n" +
            "Order by 2 DESC;\n";

    static String GET_DEALER_NET_QUANTITIES_QUERY = "select counterparty_name, SUM(IF(deal_type=\"B\", " +
            "deal_quantity, (-1)*deal_quantity)) as net_quantity, instrument_name\n" +
            "from %s\n" +
            "where counterparty_name=?\n" +
            "group by counterparty_name, instrument_name";

    static String GET_LAST_PRICES_QUERY = "Select Round(AVG(a.deal_amount),2) as amount,b.instrument_name from %s a JOIN\n" +
            "(\n" +
            "Select instrument_name, deal_type, max(deal_time) as time from %s \n" +
            "Group by instrument_name,deal_type \n" +
            ")  b ON a.instrument_name=b.instrument_name and a.deal_type=b.deal_type and b.time=a.deal_time\n" +
            "Where a.deal_type = ?\n" +
            "group by instrument_name;";
}
