package com.db.demomidtier;

import java.sql.Connection;
import java.sql.SQLException;

public class DBConnector {
    static private DBConnector itsSelf = null;

    private Connection itsConnection;

    static public DBConnector getConnector() {
        if (itsSelf == null)
            itsSelf = new DBConnector();
        return itsSelf;
    }

    private DBConnector() {
    }

    public boolean isConnected() {
        try {
            if (itsConnection == null || !itsConnection.isValid(1)) {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Connection getConnection() {
        return itsConnection;
    }

    public boolean connect(Connection connection) {
        itsConnection = connection;
        return itsConnection != null;
    }
}
